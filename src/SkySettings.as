package 
{
	/**
	 * ...
	 * @author Adam Testerman
	 */
	public class SkySettings
	{
		public static const GRAVITY:int =	 						10;					//pixels per frame
		public static const GRAVITY_DECAY:Number =					.9;
		public static const START_LEVEL:int = 						1;					//game starts at this level (change for testing purposes)
		public static const START_HEARTS:int = 						5;					//pc starts with this many hearts
		public static const RAIN_TIMER_MAX:int =					15;					//new debris every so often
		public static const SECONDARY_COLLIDE_TIMER_MAX:int =		5;					//max amount of time (in frames) between non-floor collision checking
		
		public static const POWERUP_VALUE_FLOAT:int =				7;					//gravity reduction from regular float umbrella
		public static const POWERUP_VALUE_FLOAT_ULTRA:int =			5;					//gravity reduction from ultra umbrella
		public static const POWERUP_VALUE_JUMP:int =				-5;					//jump boost from regular jump umbrella
		public static const POWERUP_VALUE_JUMP_ULTRA:int =			-3;					//jump boost from ultra umbrella
		public static const POWERUP_VALUE_SPEED:Number =			1.3;				//speed boost from regular speed boost umbrella
		public static const POWERUP_VALUE_SPEED_ULTRA:Number =		1.15;				//speed boost from ultra umbrella
		public static const POWERUP_VALUE_REGEN_ULTRA:int =			300;				//how long between ultra umbrella regen (in frames)
	}
}