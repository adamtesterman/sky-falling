﻿package
{
	import flash.display.*;
	import flash.events.*;
	import flash.media.Sound;
	import flash.text.*;
	import flash.ui.Keyboard;
	
	import events.GameEvent;
	
	import ui.*;
	
	/**
	 * Document class for "The Sky is Falling".
	 * 
	 * @author Adam Testerman
	 */
	public class SkyMain extends GameBase 
	{
		//PROPERTIES / Constants (Audio Labels)
		private const SFX_UMBRELLA_PICKUP:String =			"UmbrellaPickup";
		private const SFX_HEART_PICKUP:String =				"HeartPickup";
		private const SFX_HIT_UMBRELLA:String =				"HitUmbrella";
		private const SFX_HIT_PC:String =					"HitPC";
		private const SFX_HIT_PC_GAME_OVER:String =			"HitPCGameOver";
		private const SFX_HIT_PC_FALL:String =				"HitPCFall";
		private const SFX_JUMP:String =						"Jump";
		private const SFX_WIN:String =						"Win";
		
		private const BGM_CITY:String =						"City";
		private const BGM_CLIFFS:String =					"Cliffs";
		private const BGM_DESERT:String =					"Desert";
		
		//PROPERTIES / Variables
		private var _currentLevelNum:int;					//current level
		private var _fadeDirection:int = 1;					//determines whether fade-in/fade-out black screen is fading in or out
		private var _transitioning:Boolean;					//whether or not the screen is _transitioning to the next level
		private var _rainRotAmount1:Number;					//how much the first rain drop rotates as it falls
		private var _rainRotAmount2:Number;					//how much the second rain drop rotates as it falls
		private var _levelStartX:int;						//position of the level when it starts
		private var _shiftHeld:Boolean;						//whether or not the shift key is being held
		private var _raining:Boolean;						//controls whether to stop the falling debris
		private var _rainTimerCur:int;						//how long before the next piece of debris falls
		private var _secondaryCollideTimerCur:int;			//timer between collisions checks with objects that don't have to be checked every frame
		
		//PROPERTIES / Objects
		private var _screenFader:SkyScreenFader;				//fade-in/fade-out black screen
		
		//PROPERTIES / Game Screens
		private var _screenGameOver:SkyScreenGameOver;
		private var _screenHelp:SkyScreenHelp;
		private var _screenLoading:SkyScreenLoading;
		private var _screenMainMenu:SkyScreenMainMenu;
		
		//PROPERTIES / Lists
		private var _rainDrops:Array = [];					//list of all currently falling debris
		private var _hearts:Array = [];					//array keeping track of PC's hearts
		private var _umbrellaList:Array = [];				//array keeping track of PC's powerup umbrella's durability
		
		
		//=================================
		//		VITAL METHODS
		//=================================
		
		/**
		 * Constructor. Creates a new instance of <code>SkyMain</code>.
		 */
		public function SkyMain():void 
		{
			
		}
		
		
		/**
		 * Deconstructor. Removes all dynamically created objects, event listeners, and other data in order to prepare this object
		 * for garbage collection.
		 */
		private function killMe(even:MouseEvent = null):void 
		{
			//RemoveDynamicChildren();
			RemoveEventListeners();
		}
		
		
		/**
		 * (Override Method)
		 * 
		 * Calls the game's <code>initGame()</code> method after the game has been added to the stage. Each game should then perform
		 * game-specific tasks in order to proceed, such as opening the main menu.
		 */
		protected override function initGame(event:Event = null):void 
		{
			gameLevel.stop();
			
			OpenLoading();
		}
		
		
		/**
		 * Adds all event listeners that work with the entirety of this object.
		 */
		private function AddEventListeners():void 
		{
			addEventListener(Event.ENTER_FRAME, GameLoop);
			stage.addEventListener(KeyboardEvent.KEY_DOWN, HandleKeyDown);
			stage.addEventListener(KeyboardEvent.KEY_UP, HandleKeyUp);
		}
		
		
		/**
		 * Removes all event listeners that work with the entirety of this object.
		 */
		private function RemoveEventListeners():void 
		{
			removeEventListener(Event.ENTER_FRAME, GameLoop);
			stage.removeEventListener(KeyboardEvent.KEY_DOWN, HandleKeyDown);
			stage.removeEventListener(KeyboardEvent.KEY_UP, HandleKeyUp);
			removeEventListener(Event.ENTER_FRAME, ResetPCPosition);
			removeEventListener(Event.ENTER_FRAME, TransitionScreen);
		}
		
		
		/**
		 * Starts or restarts the first level of the game.
		 */
		private function StartGame(event:Event = null):void 
		{
			CloseAllScreens();
			
			//New properties
			_currentLevelNum = SkySettings.START_LEVEL;
			var _levelCaption:SkyLevelCaption = new SkyLevelCaption(String(_currentLevelNum));
			_raining = true;							//begin debris fall
			_rainTimerCur = 0;							//reset debris fall time
			
			//New methods
			SetGameState(GameBase.GAME_PLAYING);
			GoToLevel(_currentLevelNum);
			ModifyHearts(SkySettings.START_HEARTS);		//add hearts to PC
			pc.resetParams();							//initialize PC's parameters
			se.playMusic(BGM_CITY);
			
			//Inherited methods
			addChild(_levelCaption);
		}
		
		
		/**
		 * Standard game loop.
		 */
		private function GameLoop(event:Event):void 
		{
			//Only perform these actions if the screen isn't doing something else
			if (!_transitioning  && gameState == GameBase.GAME_PLAYING)
			{
				UpdatePC();
				UpdateRainDrops();
				
				DetectCollisions();
			}
		}
		
		
		//=================================
		//		PRIVATE METHODS
		//=================================
		
		/**
		 * Adds the specified powerup to the PC and performs actions based on which powerup was added.
		 * 
		 * @param powerupType	The powerup to add, as a string. The static constants found in <code>SkyPC</code> should be used.
		 */
		private function AddPowerup(powerupType:String):void 
		{
			pc.addPowerup(powerupType);
			
			//Add umbrella durability if possible, based on powerup type
			if 		(powerupType == SkyPC.POWERUP_FLOAT && _umbrellaList.length < 3) 	ModifyUmbrellas(3 - _umbrellaList.length);
			else if (powerupType == SkyPC.POWERUP_JUMP && _umbrellaList.length < 2) 	ModifyUmbrellas(2 - _umbrellaList.length);
			else if (powerupType == SkyPC.POWERUP_SHIELD && _umbrellaList.length < 5) 	ModifyUmbrellas(5 - _umbrellaList.length);
			else if (powerupType == SkyPC.POWERUP_SPEED && _umbrellaList.length < 3) 	ModifyUmbrellas(3 - _umbrellaList.length);
			else if (powerupType == SkyPC.POWERUP_ULTRA && _umbrellaList.length < 4) 	ModifyUmbrellas(4 - _umbrellaList.length);
		}
		
		/**
		 * Creates a new piece of falling debris and adds it to the list of falling debris.
		 */
		private function CreateRainDrop():void 
		{
			//trace("creating new debris");
			
			//Raindrop x determined by PC's speed and direction
			var newDropX:int;
			if 		(pc.dx > 0) 	newDropX = Math.random() * (loaderInfo.width / 2) + loaderInfo.width / 2;
			else if (pc.dx < 0) 	newDropX = Math.random() * (-loaderInfo.width) + loaderInfo.width / 2;				
			else if (pc.dx == 0)	newDropX = Math.random() * loaderInfo.width;
			
			var newDrop:SkyRainDrop = new SkyRainDrop(newDropX);
			addChild(newDrop);
			
			_rainDrops.push(newDrop);
		}
		
		/**
		 * Destroys all debris and prepares them for garbage collection.
		 */
		private function DestroyAllRainDrops():void 
		{
			for (var i:int = _rainDrops.length - 1; i >= 0; i--) 
			{
				DestroyRainDrop(_rainDrops[i]);
			}
		}
		
		/**
		 * Destroys the specified piece of debris and prepares it for garbage collection.
		 * 
		 * @param	drop	The piece of debris to be destroyed.
		 */
		private function DestroyRainDrop(drop:SkyRainDrop):void 
		{
			if (drop)
			{
				drop.killMe();
				
				if (drop.parent) drop.parent.removeChild(drop);
				
				if (_rainDrops.indexOf(drop) > -1) _rainDrops.splice(_rainDrops.indexOf(drop), 1);
			}
		}
		
		/**
		 * Determines whether or not the PC is colliding with something and reacts accordingly.
		 */
		private function DetectCollisions():void 
		{
			var i:int;
			
			//PC against floor colliders
			var thisCollider:SkyColliderFloor;
			var collided:Boolean;
			for (i = 1; gameLevel["fCL" + _currentLevelNum + "N" + i] != null; i++)
			{
				thisCollider = gameLevel["fCL" + _currentLevelNum + "N" + i];
				
				//If PC is hitting this collider, stop falling
				if (pc.boundingBox.hitTestObject(thisCollider))
				{
					//Only stay on the collider if moving downward
					if (pc.y > thisCollider.y && pc.y < thisCollider.y + thisCollider.height && pc.dy > 0)
					{
						collided = true;
						
						break;
					}
				}
			}
			
			//Check results after all colliders have been tested
			if (collided)
			{
				pc.y = thisCollider.y;
				
				pc.canJump = true;
				pc.curJumpPower = 0;
				
				if (pc.dx == 0) pc.animState = SkyPC.ANIM_STATE_IDLE;	//if not moving, animation is idle
				else pc.animState = SkyPC.ANIM_STATE_RUNNING;			//otherwise, animation is running
			}
			//If PC is NOT on a floor, it is jumping/falling
			else
			{
				pc.canJump = false;
				pc.animState = SkyPC.ANIM_STATE_JUMPING;
			}
			
			//PC against walls
			if ((gameLevel.wC1 && pc.boundingBox.hitTestObject(gameLevel.wC1)) || (gameLevel.wC2 && pc.boundingBox.hitTestObject(gameLevel.wC2)))
			{
				//trace("hitting wall");
				
				//Move character
				gameLevel.x += pc.dx;	//move the "camera" with the player, always centered on the player (the game level simulates the camera)
				
				//Move rain
				for (i = 0; i < _rainDrops.length; i++) 
				{
					_rainDrops[i].x += pc.dx;
				}
				
				//Move tier collider
				tierCollider.x += pc.dx;
			}
			
			//Check the rest of these every so often			
			_secondaryCollideTimerCur++;
			if (_secondaryCollideTimerCur == SkySettings.SECONDARY_COLLIDE_TIMER_MAX)
			{
				_secondaryCollideTimerCur = 0;
				
				//Tier transitions
				if (pc.boundingBox.hitTestObject(tierCollider))
				{
					TransitionScreen();
					_transitioning = true;
				}
				
				//Special colliders (for cut scenes and such)
				/*if (gameLevel["bunker" + _currentLevelNum] && pc.boundingBox.hitTestObject(gameLevel["bunker" + _currentLevelNum]))
				{
					var bunkerObject:MovieClip = gameLevel["bunker"+_currentLevelNum];
					
					if (bunkerObject.currentFrame == 1)											//bunker is on its first frame
					{								
						stage.removeEventListener(KeyboardEvent.KEY_DOWN, HandleKeyDown);		//disallow key presses
						pc.dx = 0;																//disallow PC movement
						_raining = false;														//stop _raining
						
						gameLevel["bunker"+_currentLevelNum].gotoAndPlay(2);					//continue bunker's animation
					}
					else if (bunkerObject.currentFrame == bunkerObject.totalFrames)				//bunker is on its last frame
					{	
						stage.addEventListener(KeyboardEvent.KEY_DOWN, HandleKeyDown);			//allow key presses
						_raining = true;														//start _raining
						
						gameLevel["bunker"+_currentLevelNum] = null;							//prevent this code from being called again
					}
				}*/
				
				//Tier transition near ultrabrella
				/*if (_currentLevelNum == 7)	
				{		
					if (pc.x > gameLevel.x + 2320) tierCollider.x = gameLevel.x + 2920;
					else tierCollider.x = gameLevel.x + 1300;
				}*/
				
				//Bottom of screen
				if (pc.y > loaderInfo.height + 50)
				{
					if (_hearts.length > 1)
					{
						se.playSound(SFX_HIT_PC_FALL);
					}
					else
					{
						se.playSound(SFX_HIT_PC_GAME_OVER);
					}
					
					ModifyHearts(-1);
					
					if (_hearts.length == 0) GameOver();
					
					if (gameState == GameBase.GAME_PLAYING)
					{
						pc.y = -75;
						stage.removeEventListener(KeyboardEvent.KEY_DOWN, HandleKeyDown);
						stage.removeEventListener(KeyboardEvent.KEY_UP, HandleKeyUp);
						addEventListener(Event.ENTER_FRAME, ResetPCPosition);	//Put PC back to the start of the level
					}
					else return;
				}
				
				//Powerups/objects
				var hitObject:MovieClip;
				for (i = 1; gameLevel["objL" + _currentLevelNum + "N" + i] != null; i++)
				{
					hitObject = gameLevel["objL" + _currentLevelNum + "N" + i];
					if (pc.hitTestObject(hitObject))
					{
						if (hitObject is SkyFloatingHeart)
						{
							se.playSound(SFX_HEART_PICKUP);
							ModifyHearts(1);
						}
						else if (hitObject is SkyFloatingDuctTape)
						{
							se.playSound(SFX_HEART_PICKUP);
							ModifyUmbrellas(1);
						}
						else if (hitObject is SkyFloatingFloatbrella)
						{
							se.playSound(SFX_UMBRELLA_PICKUP);
							AddPowerup(SkyPC.POWERUP_FLOAT);
						}
						else if (hitObject is SkyFloatingShieldbrella)
						{
							se.playSound(SFX_UMBRELLA_PICKUP);
							AddPowerup(SkyPC.POWERUP_SHIELD);
						}
						else if (hitObject is SkyFloatingJumpbrella)
						{
							se.playSound(SFX_UMBRELLA_PICKUP);
							AddPowerup(SkyPC.POWERUP_JUMP);
						}
						else if (hitObject is SkyFloatingSpeedbrella)
						{
							se.playSound(SFX_UMBRELLA_PICKUP);
							AddPowerup(SkyPC.POWERUP_FLOAT);
						}
						else if (hitObject is SkyFloatingUltrabrella)
						{
							se.playSound(SFX_UMBRELLA_PICKUP);
							AddPowerup(SkyPC.POWERUP_ULTRA);
						}
						else trace("Error in object detection");
						
						if (hitObject.parent) hitObject.parent.removeChild(hitObject);
					}
				}
				
				//Debris
				var thisDrop:SkyRainDrop;
				for (i = _rainDrops.length - 1; i >= 0; i--) 
				{
					thisDrop = _rainDrops[i];
					
					//Debris hits PC's powerup
					if (!pc.invincible && pc.powerup && pc.powerup.hitTestObject(thisDrop) && thisDrop.currentLabel == null)
					{
						se.playSound(SFX_HIT_UMBRELLA);
						ModifyUmbrellas(-1);
						
						DestroyRainDrop(thisDrop);
						
						return;
					}
					//Debris hits PC
					else if (pc.boundingBox.hitTestObject(thisDrop))
					{
						//Extra heart
						if 		(thisDrop.currentLabel == "heart")
						{
							se.playSound(SFX_HEART_PICKUP);
							ModifyHearts(1);
						}
						//Extra umbrella durability
						else if (thisDrop.currentLabel == "ducttape" && pc.powerup)
						{
							se.playSound(SFX_HEART_PICKUP);
							ModifyUmbrellas(1);
						}
						//Umbrella powerups
						else if (thisDrop.currentLabel != null && !pc.ultra)
						{
							se.playSound(SFX_UMBRELLA_PICKUP);
							AddPowerup(thisDrop.currentLabel);
						}
						//Regular debris
						else
						{
							if (!pc.invincible)
							{
								if (_hearts.length > 1) se.playSound(SFX_HIT_PC);
								else se.playSound(SFX_HIT_PC_GAME_OVER);
								ModifyHearts(-1);
								
								if (_hearts.length == 0) GameOver();						
								else pc.flashDamage();
							}
							else return;
						}
						
						DestroyRainDrop(thisDrop);
						
						return;
					}
				}
			}
		}
		
		/**
		 * Removes all onscreen objects and opens the game over screen.
		 */
		private function GameOver():void 
		{
			SetGameState(GameBase.GAME_OVER);
			
			se.stopAllSounds();
			
			OpenGameOver();
		}
		
		/**
		 * Has the map go to a specific level and initializes all of that level's content.
		 */
		private function GoToLevel(level:int):void 
		{
			gameLevel.gotoAndStop(level);
			
			//Game level position / level start X
			switch(level)
			{
				case 1:
				case 3:
				case 5:
				case 6:
				case 7:
				case 8:
				case 9:
				case 10:
				case 11:
				case 12:
				case 13: _levelStartX = 500;  break;				
				case 2:
				case 14: _levelStartX = -2500; break;
				case 3:
				case 16: _levelStartX = 2300; break;
				case 4:  _levelStartX = -2350; break;
			}
			gameLevel.x = _levelStartX;
			
			//PC start position / pc.y
			switch (level)
			{
				case 1:
				case 13: pc.y = 280; break;
				case 2:
				case 3:
				case 4:
				case 5:
				case 14:
				case 15:
				case 16: pc.y = -75; break;
				case 6:
				case 7:
				case 8:
				case 9:
				case 10:
				case 11:
				case 12: pc.y = 380; break;
			}
			
			//Tier collider positioning
			switch(level)
			{
				case 1:
				case 13: tierCollider.x = gameLevel.x + 2700; 	tierCollider.y = 405; break;
				case 2:
				case 4:
				case 14:
				case 16: tierCollider.x = gameLevel.x - 340; 	tierCollider.y = 405; break;
				case 3:
				case 15: tierCollider.x = gameLevel.x + 2480; 	tierCollider.y = 405; break;
				case 6:  tierCollider.x = gameLevel.x + 2880;	tierCollider.y = 260; break;
				case 5:
				case 9:
				case 10:
				case 11: tierCollider.x = gameLevel.x + 2920; 	tierCollider.y = 260; break;
				case 12: tierCollider.x = gameLevel.x - 1000;	tierCollider.y = -90; break;
				case 7:  tierCollider.x = gameLevel.x + 1300;	tierCollider.y = 260; break;
				case 8:  tierCollider.x = gameLevel.x + 2920;	tierCollider.y = -20; break;
			}
			
			//Tier collider size
			switch (level)
			{
				case 1:
				case 2:
				case 4:
				case 13:
				case 14:
				case 15:
				case 16: tierCollider.width = 200; tierCollider.height = 20; break;
				case 3:  tierCollider.width = 400; tierCollider.height = 20; break;
				case 5:
				case 6:
				case 7:
				case 9:
				case 10:
				case 11:
				case 12: tierCollider.width = 20;  tierCollider.height = 120; break;
				case 8:	 tierCollider.width = 20;  tierCollider.height = 440; break;
			}
			
			//Wall collider visibility
			switch (level)
			{
				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
				case 6:
				case 7:
				case 14:
				case 15:
				case 16: gameLevel.wC1.visible = true; gameLevel.wC2.visible = true;  break;
				case 8:  gameLevel.wC1.visible = true; gameLevel.wC2.visible = false; break;
				case 9:
				case 10:
				case 11:
				case 12: gameLevel.wC1.visible = false; gameLevel.wC2.visible = false; break;
				case 13: gameLevel.wC1.visible = false; gameLevel.wC2.visible = true;  break;
			}
			
			//Wall collider frame
			if (level <= 12)
			{
				gameLevel.wC1.gotoAndStop(1);
				gameLevel.wC2.gotoAndStop(1);
			}
			else
			{
				gameLevel.wC1.gotoAndStop(2);
				gameLevel.wC2.gotoAndStop(2);
			}
			
			//Music
			if (level == 1)
			{
				se.playMusic(BGM_CITY);
			}
			else if (level == 9)
			{
				se.stopMusic();
				se.playMusic(BGM_DESERT);
			}
			else if (level == 13)
			{
				se.stopMusic();
				se.playMusic(BGM_CLIFFS);
			}
		}
		
		/**
		 * Called when the user presses a keyboard key and does things based on which key was pressed.
		 */
		private function HandleKeyDown(event:KeyboardEvent):void 
		{
			//Left or right arrow key
			if (event.keyCode == Keyboard.LEFT || event.keyCode == Keyboard.RIGHT)
			{
				pc.scaleX = -(38 - event.keyCode);	//Left (37) = -1, Right (39) = 1
				pc.dx = pc.runSpeed * pc.scaleX;	//Determines speed and direction based on constant value and the X scale (-1 or 1)
			}
			//Spacebar
			else if (event.keyCode == Keyboard.SPACE && pc.canJump)
			{
				se.playSound(SFX_JUMP);
				pc.curJumpPower = pc.maxJumpPower;
			}
			
			//Shift
			if (event.keyCode == Keyboard.SHIFT) _shiftHeld = true;
		}
		
		/**
		 * Called when the user releases a keyboard key and does things based on which key was released.
		 */
		private function HandleKeyUp(event:KeyboardEvent):void 
		{
			//Left or right arrow key
			if (event.keyCode == Keyboard.LEFT || event.keyCode == Keyboard.RIGHT) pc.dx = 0;
			
			//Shift
			if (event.keyCode == Keyboard.SHIFT) _shiftHeld = false;
			
			//P
			if (event.keyCode == 80) TogglePause();		//"Keyboard.P" doesn't work for some reason?? gives compile error
		}
		
		/**
		 * Called when the game data has finished loading. Removes the loading screen and opens the main menu.
		 */
		private function HandlePreloadComplete(event:Event):void 
		{
			CloseScreen(null, _screenLoading);
			
			OpenMainMenu();
		}
		
		/**
		 * Adds or subtracts a certain number of hearts to/from the PC's pool. Does not check for game over.
		 */
		private function ModifyHearts(amount:int):void 
		{
			var i:int;
			
			//Adding hearts
			if (amount > 0)
			{
				for (i = 0; i < amount; i++)
				{
					_hearts.splice(_hearts.length, 0, (new SkyFloatingHeart()));
					_hearts[_hearts.length - 1].x = 25 + (_hearts.length - 1) * 25;
					_hearts[_hearts.length-1].y = 25;
					_hearts[_hearts.length-1].stop();
					addChild(_hearts[_hearts.length - 1]);
				}
			}
			//Removing hearts
			else
			{
				amount *= -1;
				for (i = 0; i < amount; i++)
				{
					removeChild(_hearts[_hearts.length - 1]);
					_hearts.splice(_hearts.length - 1, 1);
				}
			}
		}
		
		/**
		 * Adds or subtracts a certain number of umbrella durability to/from the PC's pool
		 */
		private function ModifyUmbrellas(amount:int):void 
		{
			if (pc.powerup)
			{
				var i:int;
				
				//Adding umbrellas
				if (amount > 0)
				{
					for (i = 0; i < amount; i++)
					{
						_umbrellaList.splice(_umbrellaList.length, 0, (new SkyUmbrellaClosed()));
						_umbrellaList[_umbrellaList.length - 1].x = 25 + (_umbrellaList.length - 1) * 35;
						_umbrellaList[_umbrellaList.length - 1].y = 50;
						addChild(_umbrellaList[_umbrellaList.length - 1]);
					}
				}
				//Removing umbrellas
				else
				{
					amount *= -1;
					for (i = 0; i < amount; i++)
					{
						removeChild(_umbrellaList[_umbrellaList.length - 1]);
						_umbrellaList.splice(_umbrellaList.length - 1, 1);
						
						//Ran out of umbrella durability
						if (_umbrellaList.length == 0)
						{	
							pc.removeAllPowerups();
						}
					}
				}
			}
		}
		
		/**
		 * Completely clears all objects, event listeners, etc., and returns to the main menu.
		 */
		private function QuitToMain(event:Event = null):void 
		{
			//trace("quitting to main");
			
			SetGameState(GameBase.GAME_OVER);
			
			CloseAllScreens();
			
			DestroyAllRainDrops();
			
			//Remove hearts and umbrella display from the screen
			ModifyUmbrellas(_umbrellaList.length * -1);
			ModifyHearts(_hearts.length * -1);
			
			//Remove PC's powerups
			pc.removeAllPowerups();
			
			se.stopAllSounds();
			
			OpenMainMenu();
		}
		
		/**
		 * Continually moves the PC/level in order to put the PC back to the starting position of the level.
		 */
		private function ResetPCPosition(event:Event):void 
		{
			if (gameLevel.x < _levelStartX - 20)
			{
				//move game level RIGHT
				pc.dx = -10;
				pc.y = -75;
				pc.scaleX = 1;
			}
			else if (gameLevel.x > _levelStartX + 20)
			{
				//move game level LEFT
				pc.dx = 20;
				pc.y = -75;
				pc.scaleX = -1;
			}
			else
			{
				stage.addEventListener(KeyboardEvent.KEY_DOWN, HandleKeyDown);
				stage.addEventListener(KeyboardEvent.KEY_UP, HandleKeyUp);
				
				pc.dx = 0;
				removeEventListener(Event.ENTER_FRAME, ResetPCPosition);
				
				pc.flashDamage();
			}
		}
		
		/**
		 * Sets the state of the game to the specified value and performs actions based on the new state.
		 * 
		 * @param	state	The new game state. Value should be one of the static constants found in <code>GameBase</code>.
		 */
		private function SetGameState(state:int):void 
		{
			gameState = state;
			
			if 		(gameState == GameBase.GAME_OVER)
			{
				RemoveEventListeners();
			}
			else if (gameState == GameBase.GAME_PAUSED)
			{
				RemoveEventListeners();
			}
			else if (gameState == GameBase.GAME_PLAYING)
			{
				AddEventListeners();
			}
		}
		
		/**
		 * Toggles the game state between paused and unpaused if possible, and adds/removes event listeners accordingly.
		 */
		private function TogglePause():void 
		{
			if (gameState == GameBase.GAME_PLAYING)
			{
				gameState = GameBase.GAME_PAUSED;
				
				removeEventListener(Event.ENTER_FRAME, GameLoop);
				
				stage.removeEventListener(KeyboardEvent.KEY_DOWN, HandleKeyDown);
			}
			else if (gameState == GameBase.GAME_PAUSED)
			{
				gameState = GameBase.GAME_PLAYING;
				
				addEventListener(Event.ENTER_FRAME, GameLoop);
				
				stage.addEventListener(KeyboardEvent.KEY_DOWN, HandleKeyDown);
			}
		}
		
		/**
		 * Fades the screen in and out when transitioning between levels.
		 */
		private function TransitionScreen(event:Event = null):void 
		{
			if (!_screenFader)
			{
				_screenFader = new SkyScreenFader();
				_screenFader.alpha = 0;
				addChild(_screenFader);
				
				RemoveEventListeners();
				addEventListener(Event.ENTER_FRAME, TransitionScreen);
			}
			else
			{				
				_screenFader.alpha += .03 * _fadeDirection;
				
				if (_screenFader.alpha > 1)
				{
					_currentLevelNum++;
					GoToLevel(_currentLevelNum);
					
					_fadeDirection = -1;
				}
				else if (_screenFader.alpha < 0)
				{
					_fadeDirection = 1;
					
					removeChild(_screenFader);
					_screenFader = null;
					
					AddEventListeners();
					_transitioning = false;
					
					//Create level caption
					var levelCaption:SkyLevelCaption = new SkyLevelCaption(String(_currentLevelNum));
					addChild(levelCaption);
					
					removeEventListener(Event.ENTER_FRAME, TransitionScreen);
				}
			}
		}
		
		/**
		 * Moves the PC based on which keys are pressed.
		 */
		private function UpdatePC():void 
		{
			//Move "character" vertically, applying gravity's force
			if (pc.curJumpPower + SkySettings.GRAVITY < 0)  //if PC has a jump force behind it, decay that jump force until gravity eventually takes over
			{
				pc.curJumpPower *= SkySettings.GRAVITY_DECAY;
				pc.dy = pc.curJumpPower + SkySettings.GRAVITY;
			}
			else
			{
				pc.dy = SkySettings.GRAVITY; 				//otherwise, just keep falling at gravity's rate (NEED TO FALL FASTER LATER)
			}
			
			if (pc.dy >= 0 && _shiftHeld)	//reduce gravity's effect if the correct powerup has been acquired
			{
				if 		(pc.float) pc.dy -= SkySettings.POWERUP_VALUE_FLOAT;
				else if	(pc.ultra) pc.dy -= SkySettings.POWERUP_VALUE_FLOAT_ULTRA;
			}
			
			pc.y += pc.dy;					//always move character toward the direction with the most force (jump strength vs. gravity)
			
			//Move "character" horizontally
			gameLevel.x += -pc.dx;			//move the "camera" with the player, always centered on the player (the game level simulates the camera)
			tierCollider.x += -pc.dx;		//move tier collider
		}
		
		/**
		 * Moves the falling debris downward and rotationally
		 */
		private function UpdateRainDrops():void 
		{
			//Create more debris every so often
			_rainTimerCur++;
			if (_raining && _rainTimerCur == SkySettings.RAIN_TIMER_MAX)
			{
				_rainTimerCur = 0;
				
				CreateRainDrop();
			}
			
			//Update raindrop positions, rotation, and destroy them if offscreen
			var thisDrop:SkyRainDrop;
			for (var i:int = _rainDrops.length - 1; i >= 0; i--) 
			{
				thisDrop = _rainDrops[i];
				
				thisDrop.updateState(pc.dx);
				
				if (thisDrop.y > loaderInfo.height + 50) DestroyRainDrop(thisDrop);
			}
		}
		
		
		//=================================
		//		GAME SCREENS
		//=================================
		
		private function CloseAllScreens():void 
		{
			CloseScreen(null, _screenGameOver);
			CloseScreen(null, _screenHelp);
			CloseScreen(null, _screenLoading);
			CloseScreen(null, _screenMainMenu);
		}
		
		private function CloseScreen(event:Event = null, screen:MovieClip = null):void 
		{
			var _thisScreen:MovieClip;
			
			if (event)	_thisScreen = event.target as MovieClip;
			else 		_thisScreen = screen;
			
			if (_thisScreen && contains(_thisScreen))
			{
				if (_thisScreen.killMe) _thisScreen.killMe();
				
				if (_thisScreen.parent) _thisScreen.parent.removeChild(_thisScreen);
				
				if (_thisScreen == _screenGameOver)
				{
					_screenGameOver.removeEventListener(GameEvent.CLICK_MAIN_MENU, QuitToMain);
					
					_screenGameOver = null;
				}
				else if (_thisScreen == _screenHelp)
				{
					_screenHelp.removeEventListener(GameEvent.CLOSE_SCREEN, CloseScreen);
					
					_screenHelp = null;
				}
				else if (_thisScreen == _screenLoading)
				{
					_screenLoading.removeEventListener(GameEvent.PRELOAD_COMPLETE, HandlePreloadComplete);
					
					_screenLoading = null;
				}
				else if (_thisScreen == _screenMainMenu)
				{
					_screenMainMenu.removeEventListener(GameEvent.CLICK_HELP, OpenHelp);
					_screenMainMenu.removeEventListener(GameEvent.CLICK_PLAY, StartGame);
					
					_screenMainMenu = null;
				}
			}
		}
		
		private function OpenGameOver():void 
		{
			//If screen doesn't exist, create it first
			if (!_screenGameOver) _screenGameOver = new SkyScreenGameOver();
			
			addChild(_screenGameOver);
			
			//Add listeners for screen's button clicks
			_screenGameOver.addEventListener(GameEvent.CLICK_MAIN_MENU, QuitToMain);
		}
		
		private function OpenHelp(event:Event = null):void 
		{
			//If screen doesn't exist, create it first
			if (!_screenHelp) _screenHelp = new SkyScreenHelp();
			
			addChild(_screenHelp);
			
			//Add listeners for screen's button clicks
			_screenHelp.addEventListener(GameEvent.CLOSE_SCREEN, CloseScreen);
		}
		
		private function OpenLoading():void 
		{
			//If screen doesn't exist, create it first
			if (!_screenLoading) _screenLoading = new SkyScreenLoading();
			
			//If screen exists, bring it to front
			addChild(_screenLoading);
			
			//Add listeners for screen's button clicks
			_screenLoading.addEventListener(GameEvent.PRELOAD_COMPLETE, HandlePreloadComplete);
		}
		
		private function OpenMainMenu():void 
		{
			//If screen doesn't exist, create it first
			if (!_screenMainMenu) _screenMainMenu = new SkyScreenMainMenu();
			
			addChild(_screenMainMenu);
			
			//Add listeners for screen's button clicks
			_screenMainMenu.addEventListener(GameEvent.CLICK_HELP, OpenHelp);
			_screenMainMenu.addEventListener(GameEvent.CLICK_PLAY, StartGame);
		}
	}
}