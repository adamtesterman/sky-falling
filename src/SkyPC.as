package
{
	import flash.display.MovieClip;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Adam Testerman
	 */
	public class SkyPC extends MovieClip
	{
		//PROPERTIES / Constants (Static)
		public static const ANIM_STATE_IDLE:String =		"idle";
		public static const ANIM_STATE_RUNNING:String =		"running";
		public static const ANIM_STATE_JUMPING:String =		"jumping";
		
		public static const POWERUP_FLOAT:String =			"float";
		public static const POWERUP_SPEED:String =			"speed";
		public static const POWERUP_JUMP:String =			"jump";
		public static const POWERUP_SHIELD:String =			"shield";
		public static const POWERUP_ULTRA:String =			"ultra";
		
		public static const START_MAX_JUMP_POWER:int =		-40;
		
		//PROPERTIES / Constants
		private const DAMAGE_FLASH_TIMER_MAX:int =			30;		//length of time (in frames) to flash when damaged
		private const POWERUP_CYCLE_TIMER_MAX:int =			30;		//length of time (in frames) between each powerup graphic display
		
		//PROPERTIES / Variables
		private var _animState:String;
		private var _canJump:Boolean;
		private var _maxJumpPower:int;
		private var _curJumpPower:int;
		private var _runSpeed:Number;
		private var _dx:Number;
		private var _dy:Number;
		private var _speed:Boolean;
		private var _jump:Boolean;
		private var _float:Boolean;
		private var _shield:Boolean;
		private var _ultra:Boolean;
		private var _damageFlashTimerCur:int;
		private var _powerupCycleTimerCur:int;
		private var _umbrellaRegenTimerCur:int;				//timer for how long before an umbrella regens a durability point
		private var _invincible:Boolean;
		
		//PROPERTIES / Objects
		private var _powerup:SkyPowerup;					//pc's powerup umbrella
		
		
		//=================================
		//		VITAL METHODS
		//=================================
		
		/**
		 * Constructor. Creates a new instance of <code>SkyPC</code>.
		 */
		public function SkyPC():void 
		{
			//New methods
			resetParams();
		}
		
		
		/**
		 * Deconstructor. Removes all dynamically created objects, event listeners, and other data in order to prepare this object
		 * for garbate collection.
		 */
		private function killMe():void 
		{
			RemoveDynamicChildren();
			RemoveEventListeners();
		}
		
		
		/**
		 * Removes all event listeners that work with the entirety of this object.
		 */
		private function RemoveEventListeners():void 
		{
			removeEventListener(Event.ENTER_FRAME, UpdateRegenPowerupTimer);
			removeEventListener(Event.ENTER_FRAME, ContinueDamageFlash);
			removeEventListener(Event.ENTER_FRAME, CyclePowerupDisplay);
		}
		
		
		/**
		 * Removes all objects that were created by this object in order to prepare them for garbage collection.
		 */
		private function RemoveDynamicChildren():void 
		{
			removeAllPowerups();
		}
		
		
		//=================================
		//		PUBLIC METHODS
		//=================================
		
		/**
		 * Creates a new powerup for the PC or adds a powerup to its existing powerups.
		 * 
		 * @param powerupType	The powerup to add, as a string. The static constants found in <code>SkyPC</code> should be used.
		 */
		public function addPowerup(powerupType:String):void 
		{
			//If PC doesn't have a powerup, create a new one
			if (!_powerup)
			{
				_powerup = new SkyPowerup();
				_powerup.y = -boundingBox.height + 20;
				addChild(_powerup);
			}
			//Otherwise, start cycling between all of PC's powerups
			else
			{
				addEventListener(Event.ENTER_FRAME, CyclePowerupDisplay);
			}
			
			_powerup.gotoAndStop(powerupType);
			
			//Powerup-specific modifiers
			if 		(powerupType == SkyPC.POWERUP_FLOAT)
			{
				_float = true;
			}
			else if (powerupType == SkyPC.POWERUP_JUMP)
			{
				_jump = true;
				_maxJumpPower += SkySettings.POWERUP_VALUE_JUMP;
			}
			else if (powerupType == SkyPC.POWERUP_SHIELD)
			{
				_shield = true;
			}
			else if (powerupType == SkyPC.POWERUP_SPEED)
			{
				_speed = true;
				_runSpeed *= SkySettings.POWERUP_VALUE_SPEED;
			}
			else if (powerupType == SkyPC.POWERUP_ULTRA)
			{
				_ultra = true;
				_float = _speed = _shield = _jump = false;
				_maxJumpPower += SkySettings.POWERUP_VALUE_JUMP_ULTRA;
				_runSpeed *= SkySettings.POWERUP_VALUE_SPEED_ULTRA;
				
				addEventListener(Event.ENTER_FRAME, UpdateRegenPowerupTimer);
				
				removeEventListener(Event.ENTER_FRAME, CyclePowerupDisplay);
			}
		}
		
		/**
		 * Causes the PC to start flashing whenever it is damaged. If it is already flashing, calling this function will have no effect.
		 */
		public function flashDamage():void 
		{
			_invincible = true;
			
			_damageFlashTimerCur = 0;
			
			addEventListener(Event.ENTER_FRAME, ContinueDamageFlash);
		}
		
		/**
		 * Completely eliminates all of PC's powerups and reverts speed, jump, etc. back to normal.
		 */
		public function removeAllPowerups():void 
		{
			if (_powerup)
			{
				if (_powerup.parent) _powerup.parent.removeChild(_powerup);
				_powerup = null;
			}
			
			removePowerup(SkyPC.POWERUP_FLOAT);
			removePowerup(SkyPC.POWERUP_JUMP);
			removePowerup(SkyPC.POWERUP_SHIELD);
			removePowerup(SkyPC.POWERUP_SPEED);
			removePowerup(SkyPC.POWERUP_ULTRA);
		}
		
		/**
		 * Removes a specific type of powerup if possible and reverts any corresponding stats back to normal.
		 * 
		 * @param powerupType	The powerup to be removed, as a string. The static constants found in <code>SkyPC</code> should be used.
		 */
		public function removePowerup(powerupType:String):void 
		{
			//Powerup-specific modifiers
			if 		(powerupType == SkyPC.POWERUP_FLOAT)
			{
				_float = false;
			}
			else if (powerupType == SkyPC.POWERUP_JUMP)
			{
				if (_jump) _maxJumpPower -= SkySettings.POWERUP_VALUE_JUMP;
				
				_jump = false;
				
			}
			else if (powerupType == SkyPC.POWERUP_SHIELD)
			{
				_shield = false;
			}
			else if (powerupType == SkyPC.POWERUP_SPEED)
			{
				if (_speed) _runSpeed /= SkySettings.POWERUP_VALUE_SPEED;
				
				_speed = false;
				
			}
			else if (powerupType == SkyPC.POWERUP_ULTRA)
			{
				if (_ultra)
				{
					_maxJumpPower -= SkySettings.POWERUP_VALUE_JUMP_ULTRA;
					_runSpeed /= SkySettings.POWERUP_VALUE_SPEED_ULTRA;
				}
				
				_ultra = false;
				
				removeEventListener(Event.ENTER_FRAME, UpdateRegenPowerupTimer);
			}
		}
		
		/**
		 * Initialize/reset parameters to default states
		 */
		public function resetParams():void 
		{
			animState = SkyPC.ANIM_STATE_IDLE;
			_maxJumpPower = SkyPC.START_MAX_JUMP_POWER;
			_curJumpPower = 0;
			_runSpeed = 7;
			_dx = 0;
			_dy = 0;
			_speed = _jump = _float = _ultra = false;
			_damageFlashTimerCur = 0;
			_powerupCycleTimerCur = 0;
			_umbrellaRegenTimerCur = 0;
			_invincible = false;
			visible = true;
		}
		
		
		//=================================
		//		PRIVATE METHODS
		//=================================
		
		/**
		 * Continues flashing the PC due to being damaged and ends the flashing sequence when the timer has completed.
		 */
		private function ContinueDamageFlash(event:Event):void 
		{
			visible = !visible;
			
			_damageFlashTimerCur++;
			
			if (_damageFlashTimerCur >= DAMAGE_FLASH_TIMER_MAX)
			{
				_invincible = false;
				
				visible = true;
				
				removeEventListener(Event.ENTER_FRAME, ContinueDamageFlash);
			}
		}
		
		/**
		 * Continually cycles the visibility of each type of powerup that the PC has. For example, if the PC has jump and speed powerup,
		 * then the umbrella will cycle between the two graphics for those powerups over time. The ultra powerup overwrites all others
		 * and will only display its graphic.
		 */
		private function CyclePowerupDisplay(event:Event):void 
		{
			if (_powerup)
			{
				_powerupCycleTimerCur++;
				
				if (_powerupCycleTimerCur >= POWERUP_CYCLE_TIMER_MAX)
				{
					_powerupCycleTimerCur = 0;
					
					if 		(_powerup.currentLabel == SkyPC.POWERUP_JUMP)
					{
						if 		(_shield) 	_powerup.gotoAndStop(SkyPC.POWERUP_SHIELD);
						else if (_speed) 	_powerup.gotoAndStop(SkyPC.POWERUP_SPEED);
						else if (_float) 	_powerup.gotoAndStop(SkyPC.POWERUP_FLOAT);
					}
					else if (_powerup.currentLabel == SkyPC.POWERUP_SHIELD)
					{
						if 		(_speed)	_powerup.gotoAndStop(SkyPC.POWERUP_SPEED);
						else if (_float) 	_powerup.gotoAndStop(SkyPC.POWERUP_FLOAT);
						else if (_jump)		_powerup.gotoAndStop(SkyPC.POWERUP_JUMP);
					}
					else if (_powerup.currentLabel == SkyPC.POWERUP_SPEED)
					{
						if 		(_float) 	_powerup.gotoAndStop(SkyPC.POWERUP_FLOAT);
						else if (_jump)		_powerup.gotoAndStop(SkyPC.POWERUP_JUMP);
						else if	(_shield) 	_powerup.gotoAndStop(SkyPC.POWERUP_SHIELD);
					}
					else if (_powerup.currentLabel == SkyPC.POWERUP_FLOAT)
					{
						if 		(_jump)		_powerup.gotoAndStop(SkyPC.POWERUP_JUMP);
						else if	(_shield) 	_powerup.gotoAndStop(SkyPC.POWERUP_SHIELD);
						else if (_speed) 	_powerup.gotoAndStop(SkyPC.POWERUP_SPEED);
					}
				}
			}
			else
			{
				removeEventListener(Event.ENTER_FRAME, CyclePowerupDisplay);
			}
		}
		
		/**
		 * If the PC has the ability to regenerate powerup durability, a timer is kept track of and when it's up, an event will be
		 * dispatched to the main in order to repair some of the durability.
		 */
		private function UpdateRegenPowerupTimer(event:Event):void 
		{
			_umbrellaRegenTimerCur++;
			
			if (_umbrellaRegenTimerCur == SkySettings.POWERUP_VALUE_REGEN_ULTRA)
			{
				_umbrellaRegenTimerCur = 0;
				
				dispatchEvent(new Event(SkyEvent.UMBRELLA_REGEN_TIMER_UP));
			}
		}
		
		
		//=================================
		//		GETTER/SETTERS
		//=================================
		
		public function get animState():String 
		{
			return _animState;
		}
		
		public function set animState(value:String):void 
		{
			if (_animState != value)
			{
				_animState = value;
				
				gotoAndStop(_animState);
			}
		}
		
		public function get canJump():Boolean 
		{
			return _canJump;
		}
		
		public function set canJump(value:Boolean):void 
		{
			_canJump = value;
		}
		
		public function get curJumpPower():int 
		{
			return _curJumpPower;
		}
		
		public function set curJumpPower(value:int):void 
		{
			_curJumpPower = value;
		}
		
		public function get dx():Number 
		{
			return _dx;
		}
		
		public function set dx(value:Number):void 
		{
			_dx = value;
		}
		
		public function get dy():Number 
		{
			return _dy;
		}
		
		public function set dy(value:Number):void 
		{
			_dy = value;
		}
		
		public function get float():Boolean 
		{
			return _float;
		}
		
		public function set float(value:Boolean):void 
		{
			_float = value;
		}
		
		public function get invincible():Boolean 
		{
			return _invincible;
		}
		
		public function get jump():Boolean 
		{
			return _jump;
		}
		
		public function set jump(value:Boolean):void 
		{
			_jump = value;
		}
		
		public function get maxJumpPower():int 
		{
			return _maxJumpPower;
		}
		
		public function get powerup():SkyPowerup 
		{
			return _powerup;
		}
		
		public function set powerup(value:SkyPowerup):void 
		{
			_powerup = value;
		}
		
		public function get runSpeed():int 
		{
			return _runSpeed;
		}
		
		public function get shield():Boolean 
		{
			return _shield;
		}
		
		public function set shield(value:Boolean):void 
		{
			_shield = value;
		}
		
		public function get speed():Boolean 
		{
			return _speed;
		}
		
		public function get ultra():Boolean 
		{
			return _ultra;
		}
		
		public function set ultra(value:Boolean):void 
		{
			_ultra = value;
		}
	}
}