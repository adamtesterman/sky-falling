package ui
{
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import events.GameEvent;
	
	/**
	 * ...
	 * @author Adam Testerman
	 */
	public class SkyScreenGameOver extends MovieClip
	{
		//=================================
		//		VITAL METHODS
		//=================================
		
		/**
		 * Constructor. Creates a new instance of <code>SkyScreenGameOver</code>.
		 */
		public function SkyScreenGameOver():void 
		{
			//New methods
			AddEventListeners();
		}
		
		
		/**
		 * Deconstructor. Removes all dynamically created objects, event listeners, and other data in order to prepare this object
		 * for garbate collection. This object's parent should take care of removing it as a child.
		 */
		public function killMe():void 
		{
			RemoveEventListeners();
		}
		
		
		/**
		 * Adds all event listeners that work with the entirety of this object.
		 */
		private function AddEventListeners():void 
		{
			btnMain.addEventListener(MouseEvent.CLICK, HandleMainClicked);
		}
		
		
		/**
		 * Removes all event listeners that work with the entirety of this object.
		 */
		private function RemoveEventListeners():void 
		{
			btnMain.removeEventListener(MouseEvent.CLICK, HandleMainClicked);
		}
		
		
		//=================================
		//		PRIVATE METHODS
		//=================================
		
		/**
		 * Dispatches an event to the main so this screen can be closed.
		 */
		private function HandleMainClicked(event:MouseEvent):void 
		{
			dispatchEvent(new Event(GameEvent.CLICK_MAIN_MENU));
		}
	}
}