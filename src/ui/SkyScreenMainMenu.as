package ui
{
	import flash.display.*;
	import flash.events.*;
	
	import events.GameEvent;
	
	/**
	 * ...
	 * @author Adam Testerman
	 */
	public class SkyScreenMainMenu extends MovieClip
	{
		//PROPERTIES / Constants
		private const RAIN_TIMER_MAX:int =			15;		//new piece of debris falls every so often
		
		//PROPERTIES / Variables
		private var _rainTimerCur:int;
		
		//PROPERTIES / Lists
		private var _rainDrops:Array = [];			//list of all falling debris
		
		
		//=================================
		//		VITAL METHODS
		//=================================
		
		/**
		 * Constructor. Creates a new instance of <code>SkyScreenMainMenu</code>.
		 */
		public function SkyScreenMainMenu():void 
		{
			//New methods
			AddEventListeners();
		}
		
		
		/**
		 * Deconstructor. Removes all dynamically created objects, event listeners, and other data in order to prepare this object
		 * for garbate collection. This object's parent should take care of removing it as a child.
		 */
		public function killMe(event:MouseEvent = null):void 
		{
			RemoveDynamicChildren();
			RemoveEventListeners();
		}
		
		
		/**
		 * Adds all event listeners that work with the entirety of this object.
		 */
		private function AddEventListeners():void 
		{
			addEventListener(Event.ENTER_FRAME, UpdateRainDrops);
			
			btnStartGame.addEventListener(MouseEvent.CLICK, HandleStartGameClicked);
			btnHelp.addEventListener(MouseEvent.CLICK, HandleHelpClicked);
		}
		
		
		/**
		 * Removes all event listeners that work with the entirety of this object.
		 */
		private function RemoveEventListeners():void 
		{
			removeEventListener(Event.ENTER_FRAME, UpdateRainDrops);
			
			btnStartGame.removeEventListener(MouseEvent.CLICK, HandleStartGameClicked);
			btnHelp.removeEventListener(MouseEvent.CLICK, HandleHelpClicked);
		}
		
		
		/**
		 * Removes all objects that were created by this object, preparing them for garbage collection.
		 */
		private function RemoveDynamicChildren():void 
		{
			//Remove rain drops
			var _thisDrop:SkyRainDrop;
			for (var i:int = _rainDrops.length - 1; i >= 0; i--)
			{
				DestroyRainDrop(_rainDrops[i]);
				_rainDrops[i] = null;
			}
			
			_rainDrops = null;
		}
		
		
		//=================================
		//		PRIVATE METHODS
		//=================================
		
		/**
		 * Create a random piece of debris so it can fall.
		 */
		private function CreateRainDrop():void 
		{
			var _newRainDrop:SkyRainDrop = new SkyRainDrop(Math.random() * loaderInfo.width);
			_rainDrops.push(_newRainDrop);
			addChildAt(_newRainDrop, 1);
		}
		
		/**
		 * Completely removes the specified piece of debris, preparing it for garbage collection.
		 * 
		 * @param rainDrop	The piece of debris to be removed.
		 */
		private function DestroyRainDrop(rainDrop:SkyRainDrop):void 
		{
			if (rainDrop)
			{
				rainDrop.killMe();
				
				if (rainDrop.parent) rainDrop.parent.removeChild(rainDrop);
				
				if (_rainDrops.indexOf(rainDrop) > -1) _rainDrops.splice(_rainDrops.indexOf(rainDrop), 1);
			}
		}
		
		/**
		 * Dispatches an event to the main so this screen can be closed and the game can be started.
		 */
		private function HandleStartGameClicked(event:MouseEvent):void 
		{
			dispatchEvent(new Event(GameEvent.CLICK_PLAY));
		}
		
		/**
		 * Dispatches an event to the main so the help screen can be shown.
		 */
		private function HandleHelpClicked(event:MouseEvent):void 
		{
			dispatchEvent(new Event(GameEvent.CLICK_HELP));
		}
		
		/**
		 * Continually have objects fall from the top of the screen. If any fall below, they are destroyed.
		 */
		private function UpdateRainDrops(event:Event):void 
		{
			//Update timer and spawn new debris if needed
			_rainTimerCur--;
			
			if (_rainTimerCur <= 0)
			{
				_rainTimerCur = RAIN_TIMER_MAX;		//reset timer
				
				CreateRainDrop();
			}
			
			//Move each piece of debris downward
			for (var i:int = _rainDrops.length - 1; i >= 0; i--)
			{
				_rainDrops[i].updateState(0);
				
				if (_rainDrops[i].y > loaderInfo.height + 50) DestroyRainDrop(_rainDrops[i]);
			}
		}
	}
}