package ui
{
	import flash.display.*;
	import flash.events.*;
	
	import caurina.transitions.Tweener;
	
	/**
	 * ...
	 * @author Adam Testerman
	 */
	public class SkyLevelCaption extends MovieClip
	{
		//=================================
		//		VITAL METHODS
		//=================================
		
		/**
		 * Constructor. Creates a new instance of <code>SkyLevelCaption</code>.
		 */
		public function SkyLevelCaption(levelText:String):void 
		{
			//Inherited properties
			txtLevelText.text = levelText;
			
			//Inherited methods
			addEventListener(Event.ADDED_TO_STAGE, HandleAddedToStage);
		}
		
		
		/**
		 * Called when this object is added to the display list.
		 */
		private function HandleAddedToStage(event:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, HandleAddedToStage);
			
			TweenIn();
		}
		
		
		/**
		 * Deconstructor. Removes all dynamically created objects, event listeners, and other data in order to prepare this object
		 * for garbate collection.
		 */
		public function killMe():void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, HandleAddedToStage);
			
			if (parent) parent.removeChild(this);
		}
		
		
		//=================================
		//		PRIVATE METHODS
		//=================================
		
		//Tween into place
		private function TweenIn():void 
		{
			alpha = 0;
			x = loaderInfo.width / 2;
			y = loaderInfo.height / 2 + 100;
			
			Tweener.addTween(this, { y:loaderInfo.height / 2, time:.5 } );										//ease in y position
			Tweener.addTween(this, { alpha:1, time:.5, transition:"linear" } );									//ease in alpha
			Tweener.addTween(this, { y:loaderInfo.height / 2 - 100, delay:1.5, time:.5 } );						//ease out y position, delayed
			Tweener.addTween(this, { alpha:0, delay:1.5, time:.5, transition:"linear", onComplete:killMe } );	//ease out alpha, delayed
		}
	}
}