package ui
{
	import flash.display.MovieClip;
	import flash.events.Event;
	
	import events.GameEvent;
	
	/**
	 * ...
	 * @author Adam Testerman
	 */
	public class SkyScreenLoading extends MovieClip
	{
		//=================================
		//		VITAL METHODS
		//=================================
		
		/**
		 * Constructor. Creates a new instance of <code>SkyScreenLoading</code>.
		 */
		public function SkyScreenLoading():void 
		{
			//New methods
			AddEventListeners();
			
			
		}
		
		
		/**
		 * Deconstructor. Removes all dynamically created objects, event listeners, and other data in order to prepare this object
		 * for garbate collection.
		 */
		public function killMe():void 
		{
			RemoveEventListeners();
		}
		
		
		/**
		 * Adds all event listeners that work with the entirety of this object.
		 */
		private function AddEventListeners():void 
		{
			addEventListener(Event.ENTER_FRAME, UpdateLoadProgress);
		}
		
		
		/**
		 * Removes all event listeners that work with the entirety of this object.
		 */
		private function RemoveEventListeners():void 
		{
			removeEventListener(Event.ENTER_FRAME, UpdateLoadProgress);
		}
		
		
		//=================================
		//		PRIVATE METHODS
		//=================================
		
		private function UpdateLoadProgress(event:Event):void 
		{
			//Get bytes loaded and bytes total
			var movieBytesLoaded:int = loaderInfo.bytesLoaded;
			var movieBytesTotal:int = loaderInfo.bytesTotal;
			
			//Convert to kilobytes so they can be displayed
			//var movieKLoaded:int = movieBytesLoaded/1024;
			//var movieKTotal:int = movieBytesTotal/1024;
			
			//Show progress bar updating
			progressBar.scaleX = movieBytesLoaded / movieBytesTotal;
			
			//Move on if done
			if (movieBytesLoaded >= movieBytesTotal) dispatchEvent(new Event(GameEvent.PRELOAD_COMPLETE));
		}
	}
}