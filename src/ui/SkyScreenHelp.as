package ui
{
	import flash.display.*;
	import flash.events.*;
	
	import events.GameEvent;
	
	/**
	 * ...
	 * @author Adam Testerman
	 */
	public class SkyScreenHelp extends MovieClip
	{
		//=================================
		//		VITAL METHODS
		//=================================
		
		/**
		 * Constructor. Creates a new instance of <code>SkyScreenHelp</code>.
		 */
		public function SkyScreenHelp():void 
		{
			//Inherited properties
			btnPrev.visible = false;
			
			//New methods
			AddEventListeners();
			
			//Inherited methods
			content.stop();
		}
		
		
		/**
		 * Deconstructor. Removes all dynamically created objects, event listeners, and other data in order to prepare this object
		 * for garbate collection. This object's parent should take care of removing it as a child.
		 */
		public function killMe():void 
		{
			RemoveEventListeners();
		}
		
		
		/**
		 * Adds all event listeners that work with the entirety of this object.
		 */
		private function AddEventListeners():void 
		{
			btnMain.addEventListener(MouseEvent.CLICK, HandleMainClicked);
			btnNext.addEventListener(MouseEvent.CLICK, HandleNextClicked);
			btnPrev.addEventListener(MouseEvent.CLICK, HandlePrevClicked);
		}
		
		
		/**
		 * Removes all event listeners that work with the entirety of this object.
		 */
		private function RemoveEventListeners():void 
		{
			btnMain.removeEventListener(MouseEvent.CLICK, HandleMainClicked);
			btnNext.removeEventListener(MouseEvent.CLICK, HandleNextClicked);
			btnPrev.removeEventListener(MouseEvent.CLICK, HandlePrevClicked);
		}
		
		
		//=================================
		//		PRIVATE METHODS
		//=================================
		
		/**
		 * Dispatches an event to the main so this screen can be closed.
		 */
		private function HandleMainClicked(event:MouseEvent):void 
		{
			dispatchEvent(new Event(GameEvent.CLOSE_SCREEN));
		}
		
		/**
		 * Show next page in the content box.
		 */
		private function HandleNextClicked(event:MouseEvent):void 
		{
			content.nextFrame();
			
			if (content.currentFrame == content.totalFrames) btnNext.visible = false;
			else btnNext.visible = true;
			
			btnPrev.visible = true;
		}
		
		/**
		 * Show next previous in the content box.
		 */
		private function HandlePrevClicked(event:MouseEvent):void 
		{
			content.prevFrame();
			
			if (content.currentFrame == 1) btnPrev.visible = false;
			else btnPrev.visible = true;
			
			btnNext.visible = true;
		}
	}
}