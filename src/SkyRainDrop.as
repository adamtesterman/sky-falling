package
{
	import flash.display.MovieClip;
	
	/**
	 * ...
	 * @author Adam Testerman
	 */
	public class SkyRainDrop extends MovieClip
	{
		//PROPERTIES / Constants
		private const SCALE_MIN:Number =			.5;
		private const SCALE_MAX:Number =			.75; 	//will always be added to scale min
		private const D_ROTATION_MAX:int =			4;
		
		//PROPERTIES / Variables
		private var _dRotation:Number;	//delta rotation, how much this drop rotates every frame
		
		
		//=================================
		//		VITAL METHODS
		//=================================
		
		/**
		 * Constructor. Creates a new instance of <code>SkyRainDrop</code>.
		 */
		public function SkyRainDrop(startX:int, startY:int = -50):void 
		{
			//New properties
			_dRotation = Math.random() * D_ROTATION_MAX;
			if (Math.random() < .5) _dRotation *= -1;		//chance of rotating opposite direction
			
			//Inherited properties
			x = startX;
			y = startY;
			scaleX = scaleY = Math.random() * SCALE_MAX + SCALE_MIN;
			
			//Inherited methods
			gotoAndStop(int(Math.random() * totalFrames) + 1);
		}
		
		
		/**
		 * Deconstructor. Removes all dynamically created objects, event listeners, and other data in order to prepare this object
		 * for garbate collection.
		 */
		public function killMe():void 
		{
			//
		}
		
		
		//=================================
		//		PUBLIC METHODS
		//=================================
		
		//Update position and rotation
		public function updateState(pcDX:Number):void 
		{
			x += -pcDX;
			y += SkySettings.GRAVITY;
			rotation += _dRotation;
		}
	}
}